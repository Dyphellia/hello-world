import axiosInstance from "@/utils/request";

interface Login {
    account: string
    password: string
}

interface Register {
    account: string
    password: string
    userGroup: string
}

export const login = (data: Login) => {
    return axiosInstance.request({
        method: 'post',
        url: '/users/checkLogin',
        data
    })
}


export const register = (data: Register) => {
    return axiosInstance.request({
        method: 'post',
        url: '/users/add',
        data
    })
}